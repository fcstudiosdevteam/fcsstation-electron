import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import FlightSimulatorOperations from "../core/FlightSimulator/FlightSimulatorOperations.js"
import LogSystem from "../Helpers/LogSystem.js";
var remote = require("electron").remote;
const { ipcRenderer } = require("electron")
var args = require("electron").remote.process.argv.slice(1);

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  constructor(private router: Router) {}


  
  ngOnInit(): void {

    ipcRenderer.on('ping', (event, argv ) => {
      LogSystem.Debug(argv);
      args = argv;
      this.ExecuteC4DProcess()
    });

    this.ExecuteC4DProcess()
  }


  ExecuteC4DProcess(){
    if(true){
      
       LogSystem.Debug(args);
       LogSystem.Debug(`Arguments Before: ${args.length}`)
      //args.push("./{**MCXPath**: **E:\\Flight Simulation Tools\\ModelConverter X**, **OutputLocation**: **F:\\System Folders\\Desktop\\MCX Test**, **ProcessType**: 2}");
      //args.push(`{**TemplateFile**: **G:\\Stroage and  Apps\\Flight Simulation Apps\\# Flight Simulation Platforms Apps #\\SceneryDevTools\\SceneryDesignTools\\ModelConverter X\\templates\\Set_Default_Opaque.mcxmat**, **OutputLocation**: **C:\\Users\\AP_Ashton_TheCreator\\AppData\\Roaming\\MAXON\\Maxon Cinema 4D R21_64C2B3BD\\prefs\\fsSdkPrefs\\mcxCustomTemplates**, **ProcessType**: 6}`)
      //args.push("./{**OutputLocation**: **C:\\Users\\SPV_s\\AppData\\Roaming\\Maxon\\Maxon Cinema 4D R22_87DECD1D\\prefs\\fsSdkPrefs**, **ProcessType**: 1}")
      args.push('./{**ObjectPath**: **F:\\System Folders\\Desktop\\C4D_Xfile_PBR_Mat\\test_electron.x**, **AttachPoints**: [{**Y**: **98.224000**, **X**: **-98.711000**, **EffectObjName**: **Effect-fx_steam1**, **Z**: **-101.019000**}, {**Y**: **98.224000**, **X**: **321.821000**, **EffectObjName**: **Effect-fx_steam1**, **Z**: **-101.019000**}, {**Y**: **98.224000**, **X**: **321.821000**, **EffectObjName**: **Effect-fx_steam1**, **Z**: **92.593000**}, {**Y**: **98.224000**, **X**: **-98.711000**, **EffectObjName**: **Effect-fx_steam1**, **Z**: **92.593000**}], **FriendName**: **This is a Text**, **Arguments**: [{**MetallicTex**: **klan_hertz_pbr.dds**, **MetallicOcclusion**: 1, **SmoothnessMode**: 1, **SmoothnessValue**: 0.8, **Detail**: 1, **EmissiveTex**: **KLAN_Hertz_LM.dds**, **AlbedoTex**: **klan_hertz_diff.dds**, **PrelitVertices**: 0, **NormalTex**: **klan_hertz_norm_fs.dds**, **EmissiveUV**: 1, **DoubleSided**: 0, **NormalUV**: 1, **AssumeVecticalNormal**: 0, **MaskedThresholdVal**: 0, **AlphaCoverageEnable**: 0, **MaterialName**: **Envio1-FSMAT**, **MetallicUV**: 1, **DetailScaleY**: 1.0, **DetailScaleX**: 1.0, **ZBias**: 0, **AlbedoDiffCol**: **0.01,1.0,0.505**, **NormalScaleY**: 1.0, **NormalScaleX**: 1.0, **AlbedoUV**: 1, **MetallicValue**: 0.0, **EmissiveMode**: 1, **RenderMode**: 1}, {**MetallicTex**: **klan_hertz_pbr.dds**, **MetallicOcclusion**: 1, **SmoothnessMode**: 1, **SmoothnessValue**: 0.0, **Detail**: 1, **EmissiveTex**: **KLAN_Hertz_LM.dds**, **AlbedoTex**: **klan_hertz_diff.dds**, **PrelitVertices**: 0, **NormalTex**: **klan_hertz_norm_fs.dds**, **EmissiveUV**: 1, **DoubleSided**: 0, **NormalUV**: 1, **AssumeVecticalNormal**: 0, **MaskedThresholdVal**: 0, **AlphaCoverageEnable**: 0, **MaterialName**: **Envio3-FSMAT**, **MetallicUV**: 1, **DetailScaleY**: 1.0, **DetailScaleX**: 1.0, **ZBias**: 0, **AlbedoDiffCol**: **1.0,1.0,1.0**, **NormalScaleY**: 1.0, **NormalScaleX**: 1.0, **AlbedoUV**: 1, **MetallicValue**: 0.0, **EmissiveMode**: 1, **RenderMode**: 1}, {**MetallicTex**: **klan_hertz_pbr.dds**, **MetallicOcclusion**: 1, **SmoothnessMode**: 1, **SmoothnessValue**: 0.0, **Detail**: 1, **EmissiveTex**: **KLAN_Hertz_LM.dds**, **AlbedoTex**: **klan_hertz_diff.dds**, **PrelitVertices**: 0, **NormalTex**: **klan_hertz_norm_fs.dds**, **EmissiveUV**: 1, **DoubleSided**: 0, **NormalUV**: 1, **AssumeVecticalNormal**: 0, **MaskedThresholdVal**: 0, **AlphaCoverageEnable**: 0, **MaterialName**: **Envio2-FSMAT**, **MetallicUV**: 1, **DetailScaleY**: 1.0, **DetailScaleX**: 1.0, **ZBias**: 0, **AlbedoDiffCol**: **0.01,1.0,0.505**, **NormalScaleY**: 1.0, **NormalScaleX**: 1.0, **AlbedoUV**: 1, **MetallicValue**: 0.0, **EmissiveMode**: 1, **RenderMode**: 1}], **GUID**: **8c4ba295-7900-46e1-b4be-e4757356a0e6**, **ProcessType**: 0, **MCXPath**: **E:\\Flight Simulation Tools\\ModelConverter X**}')
      // args.shift();
      // args.shift();
      //  LogSystem.Debug(args);
      //  LogSystem.Debug(`Arguments After: ${args.length}`)
    }

    try {
      //Get the arugments from the process and check if the result has any c4d commands.
      if (args.length > 0) {
         LogSystem.Debug("Found Arguments");
        //FlightSimulatorOperations.GenerateMDL()
        //Now that we have an argument to process lets remove all * and create a json object
        for (let index = 0; index < args.length; index++) {
          if(args[index].trim().startsWith('./{'))
        {
           LogSystem.Debug("Found C4D Argument");
          let jsonFile = this.FixData(args[index]);
          let jsonObj = JSON.parse(jsonFile);

          if(jsonObj)
          {           
            //Run the process.
            FlightSimulatorOperations.RunProcess(jsonObj);
          }
          break;
        }
          
        }
      }
    } catch (error) {
       LogSystem.Debug(error)
    }
  }

  FixData(json) {
    return json.replace(/\*{2}/g, '"').replace(/\\/g, "\\\\").replace(/(\.\/)/,"");
  }
}
