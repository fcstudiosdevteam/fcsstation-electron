import Method from "../core/FlightSimulator/Models/Method.js";
import LogSystem from "./LogSystem.js";
import MeshPart from "../core/FlightSimulator/Models/MeshPart.js";
import MaterialListData from "../core/FlightSimulator/Models/MaterialListData.js";

export default class ObjectHelpers {
  static FindUnqiueColor(s) {
    let resultSplit = s.split(",");
    var vec4 = {
      RED: parseFloat(resultSplit[0]).toFixed(6),
      GREEN: parseFloat(resultSplit[1]).toFixed(6),
      BLUE: parseFloat(resultSplit[2]).toFixed(6),
      ALPHA: (1.0).toFixed(6),
    };
    return vec4;
  }

  static FindClosingBracketLine(
    text,
    startingIndex,
    openedBracket = "{",
    closedBracket = "}"
  ) {
    let bracketCount = 1;

    for (let i = startingIndex + 1; i < text.length; i++) {
      if (text[i].indexOf(openedBracket) > -1) {
        bracketCount++;
      }

      if (text[i].indexOf(closedBracket) > -1) {
        bracketCount--;
      }

      if (bracketCount == 0) {
        return i;
      }
    }
    return -1;
  }

  static GetMethodName(methodLine) {
    var line = methodLine.trim();
    var lineFormatted = line.substring(0, line.length - 1).trim();
    return lineFormatted;
  }

  static GetFindMethod(file, name, callback) {
    LogSystem.Debug(`Getting ${name}`);
    for (let i = 0; i < file.length; i++) {
      if (file[i].trim().toLowerCase().startsWith("template")) continue;

      if (file[i].trim().toLowerCase().includes(name.toLowerCase())) {
        callback(new Method(file, i, name));
      }
    }
  }

  static GetMeshNormals(file, meshData) {
     LogSystem.Debug("Get Mesh Normals");
    this.GetFindMethod(file, "MeshNormals", (method) => {
       LogSystem.Debug("Getting Normal Data")
       LogSystem.Log(method)
       LogSystem.Log(meshData)

      for (let currentMeshData = 0; currentMeshData < meshData.length; currentMeshData++) {
        
         LogSystem.Debug("Current Mesh Data: "+ meshData[currentMeshData].Name)
        loop1:
        for (let line = 0; line < method.Content.length; line++) {
          let lineTrim = method.Content[line].trim()

          if (lineTrim.startsWith("//")) {
            let mesh = lineTrim.substring(3);
            
            if(meshData[currentMeshData].Name === mesh){
               LogSystem.Debug("Found Normal: " + meshData[currentMeshData].Name)

              for (let currentMeshDataLine = line + 1; currentMeshDataLine < method.Content.length; currentMeshDataLine++) {
                const element = method.Content[currentMeshDataLine];
                let currentLine = element.trim();
                if (currentLine.endsWith(";;")) {
                  meshData[currentMeshData].Normals.Vertices.push(currentLine);
                  LogSystem.Debug(`Name: ${mesh} || Line: ${line}`)
                  this.GetC4DNormalFaces(method,currentMeshDataLine,meshData);
                  break loop1;
                }else if (!currentLine.startsWith("//")) {
                  meshData[currentMeshData].Normals.Vertices.push(currentLine);
                } else if (currentLine.startsWith("//")) {
                  break loop1;
                }
              }
              break;
            }
          }
        }
      }
    });
  }

  static GetMeshTextureCoords(file, meshData) {
    LogSystem.Debug("Get Mesh Texture Coords");
    this.GetFindMethod(file, "MeshTextureCoords", (method) => {
       LogSystem.Debug("Getting Mesh Texture Coords Data")
       LogSystem.Debug(method)

      for (let currentMeshData = 0; currentMeshData < meshData.length; currentMeshData++) {
        
         //LogSystem.Debug("Current Mesh Data: "+ meshData[currentMeshData].Name)
        loop1:
        for (let line = 0; line < method.Content.length; line++) {
          let lineTrim = method.Content[line].trim()

          if (lineTrim.startsWith("//")) {
            let mesh = lineTrim.substring(3);
            
            if(meshData[currentMeshData].Name === mesh){
               LogSystem.Debug("Found Texture Coords: " + meshData[currentMeshData].Name)

              for (let currentMeshDataLine = line + 1; currentMeshDataLine < method.Content.length; currentMeshDataLine++) {
                const element = method.Content[currentMeshDataLine];
                let currentLine = element.trim();
                if (!currentLine.startsWith("//")) {
                  meshData[currentMeshData].TextureCoords.push(currentLine);
                } else if (currentLine.startsWith("//")) {
                  break loop1;
                }
              }
              break;
            }
          }
        }
      }

    });
  }

  static GetC4DMeshDataVertices(C4DMeshData,cb){
    LogSystem.Debug("Getting Mesh Data Vertices");
    let meshDataLines = [];
    let meshData = [];
    let currentCount = 0;
    let closingLine = 0;

    //Get List Amount
    let amount = parseInt(C4DMeshData.Content[0]);
    LogSystem.Debug(`MeshDataVert List Count: ${amount}`)
    
    
    for (let meshLine = 1; meshLine < C4DMeshData.Content.length; meshLine++) {
      let line = C4DMeshData.Content[meshLine].trim();

      meshDataLines.push(line);
      if(!line.startsWith("//")){
        currentCount += 1;
      }
      if(line.endsWith(";;")){
        closingLine = meshLine
        break;
      }
    }

    if(amount === currentCount){
      //Lets Make the objects

      console.log(meshDataLines)

      for (let meshDataLine = 0; meshDataLine < meshDataLines.length; meshDataLine++) {
        let line = meshDataLines[meshDataLine].trim();
       //console.log(`Reading Mesh Line ${meshDataLine}`)
        //Check if the line starts with a //
        if (line.startsWith("//")) {
          let lineName = line.slice(3).trim();
          let meshObject = new MeshPart(lineName);

          LogSystem.Debug(`Adding Vert Section ${lineName} at line ${meshDataLine}`)


          console.log("Getting data")
          for (let childMeshLine = meshDataLine + 1; childMeshLine < meshDataLines.length; childMeshLine++) {
            let line = meshDataLines[childMeshLine].trim();
            
            if (!line.startsWith("//")) {
              meshObject.Mesh.Vertices.push(line);
  
            }

            if(line.startsWith("//")) {
              meshData.push(meshObject);
              console.log(`Found new Mesh At line ${childMeshLine}`)
              meshDataLine = childMeshLine - 1;
              break;
            }

            if(line.endsWith(";;")){
              meshData.push(meshObject);
              break;
            }
          }
        }
      }
    }
    cb(meshData,closingLine);
  }

  static GetC4DMeshDataFaces(C4DMeshData,StartingIndex,objs,cb){
    LogSystem.Debug("Getting Mesh Data Faces");
    LogSystem.Debug(`Starting Line ${StartingIndex + 1}`);
    LogSystem.Log(C4DMeshData)
    let meshDataLines = [];
    let currentCount = 0;
    let closingLine = 0;
    let amount = 0;
    
    for (let meshLine = StartingIndex + 1; meshLine < C4DMeshData.Content.length; meshLine++) {
      let line = C4DMeshData.Content[meshLine].trim();

      if(line === ""){
        continue;
      }

      if(line.endsWith(";") && amount === 0){
        //Get List Amount
        amount = parseInt(C4DMeshData.Content[meshLine]);
        LogSystem.Debug(`MeshDataFaces List Count: ${amount}`)
        continue;
      }

      meshDataLines.push(line);
      if(!line.startsWith("//")){
        currentCount += 1;
      }
      if(line.endsWith(";;")){
        closingLine = meshLine
        break;
      }
    }

    if(currentCount === amount){
      //Lets Make the objects

      console.log(meshDataLines)

      for (let meshDataLine = 0; meshDataLine < meshDataLines.length; meshDataLine++) {
        let line = meshDataLines[meshDataLine].trim();
       
        //console.log(`Reading Mesh Line ${meshDataLine}`)
        
        //Check if the line starts with a //
        if (line.startsWith("//")) {
        
        let lineName = line.slice(3).trim();
        
          LogSystem.Debug(`Mesh Data Face Line Name: ${lineName}`)

          for (let meshPart = 0; meshPart < objs.length; meshPart++) {
            if(objs[meshPart].Name === lineName){
              console.log(`MeshPart Name: ${objs[meshPart].Name} || Current Name: ${lineName}`)

          LogSystem.Debug(`Adding Face Section ${lineName} at line ${meshDataLine}`)

          console.log("Getting data")

          for (let childMeshLine = meshDataLine + 1; childMeshLine < meshDataLines.length; childMeshLine++) {
            let line = meshDataLines[childMeshLine].trim();
            
            if (!line.startsWith("//")) {
              objs[meshPart].Mesh.Faces.push(line);
  
            }

            if(line.startsWith("//")) {
              //meshData.push(meshObject);
              console.log(`Found new Mesh At line ${childMeshLine}`)
              meshDataLine = childMeshLine - 1;
              break;
            }

            if(line.endsWith(";;")){
              //meshData.push(meshObject);
              break;
            }
          }
            }            
          }
        }
      }
    }

    cb(objs,closingLine);
  }

  static GetC4DNormalFaces(C4DMeshData,StartingIndex,objs){
    LogSystem.Debug("Getting Normal Faces");
    LogSystem.Debug(`Starting Line ${StartingIndex + 1}`);
    LogSystem.Log(C4DMeshData)
    let meshDataLines = [];
    let currentCount = 0;
    let closingLine = 0;
    let amount = 0;
    
    for (let meshLine = StartingIndex + 1; meshLine < C4DMeshData.Content.length; meshLine++) {
      let line = C4DMeshData.Content[meshLine].trim();


      if(line === ""){
        continue;
      }

      if(line.endsWith(";") && amount === 0){
        //Get List Amount
        amount = parseInt(C4DMeshData.Content[meshLine]);
        LogSystem.Debug(`Normal Faces List Count: ${amount}`)
        continue;
      }

      meshDataLines.push(line);
      if(!line.startsWith("//")){
        currentCount += 1;
      }
      if(line.endsWith(";;")){
        closingLine = meshLine
        break;
      }
    }

    if(currentCount === amount){
      //Lets Make the objects

      console.log(meshDataLines)

      for (let meshDataLine = 0; meshDataLine < meshDataLines.length; meshDataLine++) {
        let line = meshDataLines[meshDataLine].trim();
        
        //Check if the line starts with a //
        if (line.startsWith("//")) {
        
        let lineName = line.slice(3).trim();
        
          LogSystem.Debug(`Mesh Data Face Line Name: ${lineName}`)

          for (let meshPart = 0; meshPart < objs.length; meshPart++) {
            if(objs[meshPart].Name === lineName){
              console.log("Getting data")

          for (let childMeshLine = meshDataLine + 1; childMeshLine < meshDataLines.length; childMeshLine++) {
            let line = meshDataLines[childMeshLine].trim();
            
            if (!line.startsWith("//")) {
              objs[meshPart].Normals.Faces.push(line);  
            }

            if(line.startsWith("//")) {
              console.log(`Found new Mesh At line ${childMeshLine}`)
              meshDataLine = childMeshLine - 1;
              break;
            }

            if(line.endsWith(";;")){
              break;
            }
          }
            }            
          }
        }
      }
    }
  }

  static GetC4DMeshData(C4DMeshData, callback) {
    //We are trying to get all the meshes from this method and find the faces as well
    LogSystem.Debug("Getting C4DMeshData");
      this.GetC4DMeshDataVertices(C4DMeshData,(cb,line)=>{
          console.log(cb);
          console.log(`Vert Exit Line: ${line}`);
          
          this.GetC4DMeshDataFaces(C4DMeshData,line,cb,(cb1,line)=>{
            LogSystem.Log(cb1);
            callback(cb1);
            LogSystem.Debug(`Faces Exit Line: ${line}`);
          })
      })
  }

  static MeshMaterialListData(file){
    let data = new MaterialListData();
    LogSystem.Debug("Get Mesh Material List");
    this.GetFindMethod(file, "MeshMaterialList", (method) => {
       LogSystem.Debug("Getting MeshMaterialList Data")
       
       for (let line = 0; line < method.Content.length; line++) {
        let lineTrim = method.Content[line].trim()

        LogSystem.Debug(lineTrim);
        var number = parseInt(lineTrim)
        if (lineTrim.includes("{") || Number.isInteger(number) === false){
            break;
        }else if(lineTrim.endsWith(";")){
            if(line === 0){
              data.NMaterials = number
            }else if(line === 1){
              data.NFaceIndexes = number
            }else if(line > 1){
              data.FaceIndexes.push(number)
        }
      }else if(lineTrim.endsWith(",")){
        data.FaceIndexes.push(number)
      }
    }});

    console.log(data);
    return data;
  }

  static DoesExistMesh(data, name) {
    // LogSystem.Debug("Checking for Does Exist Mesh")
    var found = null;
    for (var i = 0; i < data.length; i++) {
      // LogSystem.Debug(`Comparing: ${name.trimEnd()} == ${data[i].Name}`)
      if (data[i].Name === name) {
        // LogSystem.Debug("Match Found")
        found = data[i];
        break;
      }
    }
    return found;
  }
}
