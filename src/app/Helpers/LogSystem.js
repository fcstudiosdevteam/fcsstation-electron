
let DebugMode = true;

export default class LogSystem {
   
    static get DebugMode() { return DebugMode; }
    static set DebugMode(v) { DebugMode = v; }

    static Debug(message){
        //console.log(DebugMode)
        if(DebugMode === true){
            console.log(`Debug: ${message}`);
        }
    }

    static Message(message){
        console.log(`Message: ${message}`);
    }

    static Log(message){
        console.log(message);
    }

    static ErrorMessage(err){
        console.trace(`Error: ${err}`);
    }
}