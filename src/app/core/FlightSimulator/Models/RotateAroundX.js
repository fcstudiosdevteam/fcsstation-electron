export default class RotateAroundX{
    constructor(data){
        this.Data = data;
    }

    GetTextureCoordSum(){
        let sum = 0;
        for (let i = 0; i < this.Data.MeshData.length; i++) {
                    sum +=  this.Data.MeshData[i].Mesh.Vertices.length;   
        }

        return sum;
    }

    Compile(array,cb){
        array.push("Frame RotateAroundX {"); 
        array.push("FrameTransformMatrix {"); 
        array.push("1.0, 0.0, 0.0, 0.0,"); 
        array.push("0.0, 0.0, 1.0, 0.0,"); 
        array.push("0.0, -1.0, 0.0, 0.0,"); 
        array.push("0.0, 0.0, 0.0, 1.0;"); 
        array.push("} // End Frame RotateAroundX FrameTransformMatrix"); 
        array.push(""); 
        array.push("Mesh CINEMA4D_Mesh {");
        array.push(`${this.GetTextureCoordSum()};`); 
        for (let i = 0; i < this.Data.MeshData.length; i++) {
            array.push(`//${this.Data.MeshData[i].Name}`);
            for (let texCord = 0; texCord < this.Data.MeshData[i].Mesh.Vertices.length; texCord++) { 
            array.push(`${this.Data.MeshData[i].Mesh.Vertices[texCord]}`); 
        }
        
    }
    }
}