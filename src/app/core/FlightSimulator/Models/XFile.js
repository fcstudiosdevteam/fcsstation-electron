import LogSystem from "../../../Helpers/LogSystem";
import ObjectHelpers from "../../../Helpers/ObjectHelpers";
import { Console } from "console";
import RotateAroundX from "./RotateAroundX";
let lineReader = require("line-reader");

export default class XFile{
    constructor(json){
        this.XFileLines = [];
        this.XFileParsedMethods = [];
        this.MeshData = []
        this.Methods = [];
        this.XFileCompiled=[];
        this.JSON = json;
         LogSystem.Debug(this);
    }

    Read(json,callBack){
        // TODO QuickLogger.Log(`Reading X File ${json.ObjectPath}`);
    this.XFileLines = [];
    lineReader.eachLine(json.ObjectPath.toString(),(line, last, cb)=> {
      // TODO QuickLogger.Log(`Reading XFile Line: ${line}`)
      this.XFileLines.push(line);
      if (last) {
        //Check if we have an xFile
        if (this.XFileLines) {
          LogSystem.Message("XFile has been read.")
          
          //GetMethods
          this.GetAllC4DMethods(callBack);
        }
        cb(false); // stop reading
      } else {
        cb();
      }
    });
    }

    GetAllC4DMethods(cb){
        ObjectHelpers.GetFindMethod(this.XFileLines,"CINEMA4D_Mesh", (data)=>{
            this.XFileParsedMethods.push(data);

            ObjectHelpers.GetC4DMeshData(data,(data)=>{
                this.MeshData = data;
                ObjectHelpers.GetMeshNormals(this.XFileLines,this.MeshData);
                ObjectHelpers.GetMeshTextureCoords(this.XFileLines,this.MeshData);
                this.MaterialListData = ObjectHelpers.MeshMaterialListData(this.XFileLines);
                
                cb(this); 
        })});
    }

    GenerateXHeader(){
      this.XFileCompiled.push("xof 0302txt 0064")
      this.XFileCompiled.push("//File Created By FCStudios Station")
      this.XFileCompiled.push("");
      LogSystem.Debug("Done Making XHeader");
    }

    GenerateTemplates(cb){
      LogSystem.Debug("In GenerateTemplates");
      var fs = require("fs");
      fs.readFile("./src/app/core/FlightSimulator/Data/XFileTemplate.txt",(err,data)=>{

        if(err){
          LogSystem.ErrorMessage(err.message);
        }else{
          LogSystem.Debug("Adding Templates");
          var textByLine = data.toString().split("\n")  
          for (let index = 0; index < textByLine.length; index++) {
            const element = textByLine[index];
            this.XFileCompiled.push(element);
          }
          this.XFileCompiled.push("");
          LogSystem.Debug("Done Making templates");
          cb();
        }
    });
    }

    GenerateHeader(){
      this.XFileCompiled.push("Header {"); 
      this.XFileCompiled.push(" 1;"); 
      this.XFileCompiled.push(" 0;");  
      this.XFileCompiled.push(" 1;");  
      this.XFileCompiled.push("}"); 
      this.XFileCompiled.push(""); 
    }

    GenerateGUIDToName(){
      this.XFileCompiled.push("GuidToName {"); 
      this.XFileCompiled.push(`   "${this.JSON.GUID}";`);
      this.XFileCompiled.push(`    "${this.JSON.FriendName}";`);
      this.XFileCompiled.push("}");
      this.XFileCompiled.push(""); 
    }

    GenerateFrameTransforMatrixDefault(){
      this.XFileCompiled.push("	FrameTransformMatrix {"); 
      this.XFileCompiled.push("			   1.000000, 0.000000, 0.000000, 0.000000,"); 
      this.XFileCompiled.push("			   0.000000, 1.000000, 0.000000, 0.000000,"); 
      this.XFileCompiled.push("			   0.000000, 0.000000, 1.000000, 0.000000,"); 
      this.XFileCompiled.push("			   0.000000, 0.000000, 0.000000, 1.000000;"); 
      this.XFileCompiled.push("}");
    }

    GenerateFramePart0(){
      this.XFileCompiled.push("	Frame frm_part0 {"); 
      this.GenerateFrameTransforMatrixDefault();
      this.GenerateFramePart1();
    }

    GenerateFramePart1(){
      this.XFileCompiled.push("	Frame frm_part1 {"); 
      this.GenerateFrameTransforMatrixDefault();
      var rotAroundX = new RotateAroundX(this);
      rotAroundX.Compile(this.XFileCompiled,(cb)=>{
          console.log("CreatePartData");
      });
    }

    GenerateMasterScale(cb){
      this.XFileCompiled.push("// [ Beginning of object Main Frame ]"); 
      this.XFileCompiled.push("Frame MasterScale_1 {"); 
      this.XFileCompiled.push(""); 
      this.XFileCompiled.push("	FrameTransformMatrix {"); 
      this.XFileCompiled.push("	   1.0, 0.0, 0.0, 0.0"); 
      this.XFileCompiled.push("	   0.0, 0.0, 1.0, 0.0,"); 
      this.XFileCompiled.push("	   0.0, 1.0, 0.0, 0.0,"); 
      this.XFileCompiled.push("	   0.0, 0.0, 0.0, 1.0;"); 
      this.XFileCompiled.push("}");

      this.GenerateFramePart0();

      this.XFileCompiled.push("} //End of MasterScale");
      this.XFileCompiled.push("");
      
      LogSystem.Debug("Done Making MasterScale");
      cb();
    }

    Compile(){
      LogSystem.Message("Compiling XFile!")

      //Add Header
      this.GenerateXHeader();

      //Generate Templates
      this.GenerateTemplates((cb)=>{

        //Create Header
        this.GenerateHeader();

        //Create GuidToName
        this.GenerateGUIDToName(); 

        //Create Master Scale
        this.GenerateMasterScale((cb)=>{
          LogSystem.Log(this.XFileCompiled);
          LogSystem.Debug("Done Compiling XFile")
        });        


      });


    }
}