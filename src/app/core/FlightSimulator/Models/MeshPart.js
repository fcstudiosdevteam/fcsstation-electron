import MeshObject from "./MeshObject.js";

export default class MeshPart{
    constructor(name){
        this.Name = name;
        this.Mesh = new MeshObject();
        this.Normals = new MeshObject();
        this.TextureCoords = []
    }
}

