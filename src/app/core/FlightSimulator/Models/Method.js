//This SHOULD store the parent method
//This SHOULD return the closing tag line number
//This SHOULD store any children method
//This SHOULD store any child lines between { and }
//This SHOULD compile its structure
import ObjectHelpers from "../../../Helpers/ObjectHelpers";

export default class Method {
  constructor(file, startIndex, name) {
    this.StartIndex = startIndex
    this.File = file;
    this.OpenBracket = startIndex;
    this.ClosingBracket = ObjectHelpers.FindClosingBracketLine(file,startIndex);
    this.Content = [];
    this.Name = name
    this.GetContent();
  }
  
  GetContent(){
      let pass = false;
      for (let index = this.OpenBracket + 1; index < this.ClosingBracket; index++) {
        if (this.File[index].indexOf('{') > -1){
            pass = true;
        }

        if(this.File[index].indexOf('}') > -1){
            pass = false;
        }

        if(!pass && this.File[index].indexOf('}') === -1 && this.File[index] !== " " && this.File[index] !== ""){
            this.Content.push(this.File[index].trim());
        }
      }
  }
}
