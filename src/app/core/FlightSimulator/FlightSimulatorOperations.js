import { GetProcessName, RunProcessType } from "./Enums/RunProcessType.js";
const fs = require("fs");
import XFile from "./Models/XFile.js"
import FlightSimulatorRegistrys from "./Data/FlightSimulatorRegistrys.js";
import LogSystem from "../../Helpers/LogSystem.js";
import FlightSimResObject from "./Models/FlightSimResObject.js";
import { Console } from "console";
var child = require("child_process").execFile;
var path = require("path");

export default class FlightSimulatorOperations {
  constructor() {
    this.IsPBR = false;
  }

  static RunProcess(json) {
    console.log(`Running Process: ${GetProcessName(json)}`);
    if (json.ProcessType === RunProcessType.FSPaths) {
      this.GetSims(path.join(json.OutputLocation,"FsFxPaths.json"));
    } else if (json.ProcessType === RunProcessType.MCX) {
      this.PBRProcess(json);
    } else if (json.ProcessType === RunProcessType.MCXMaterialToJson) {
      console.log("Starting Conversion")
      this.ConvertMCXTemplateToJson(json)
    } else {
      // acts as our "default"
    }
  }

  static ConvertMCXTemplateToJson(json){
    console.log("Converting Templates")
    const app = require('electron').remote.app
    let mcxPath = path.join(json.MCXPath, "ModelConverterX.exe");
    let fcStationHelperPath = path.join(app.getAppPath(), "FCStationHelperDotNet.exe");
    LogSystem.Message(`Running FCStationHelperDotNet Path: ${fcStationHelperPath}`);
    var parameters = [json.MCXPath, json.OutputLocation, "--MCXToJSON"];
    console.log(parameters);
    child(fcStationHelperPath, parameters, function (err, data) {
      if (err) {
        LogSystem.ErrorMessage(err);
        return;
      }
      LogSystem.Message(data.toString());
    });
  }

  static GetSims(outputPath) {
    console.log('Getting Sims')
    let sims = new FlightSimulatorRegistrys();
    this.GetFlightSimPath(sims.FS2004Key, (fn) => {
      this.SaveFlightSimPath("FS2004", fn, outputPath);
    });
    this.GetFlightSimPath(sims.FSXKey, (fn) => {
      this.SaveFlightSimPath("FSX", fn, outputPath);
    });
    this.GetFlightSimPath(sims.FSXSEKey, (fn) => {
      this.SaveFlightSimPath("FSXSE", fn, outputPath);
    });
    this.GetFlightSimPath(sims.P3DV1Key, (fn) => {
      this.SaveFlightSimPath("P3Dv1", fn, outputPath);
    });
    this.GetFlightSimPath(sims.P3DV2Key, (fn) => {
      this.SaveFlightSimPath("P3Dv2", fn, outputPath);
    });
    this.GetFlightSimPath(sims.P3DV3Key, (fn) => {
      this.SaveFlightSimPath("P3Dv3", fn, outputPath);
    });
    this.GetFlightSimPath(sims.P3DV4Key, (fn) => {
      this.SaveFlightSimPath("P3Dv4", fn, outputPath);
    });
    this.GetFlightSimPath(sims.P3DV5Key, (fn) => {
      this.SaveFlightSimPath("P3Dv5", fn, outputPath);
    });
  }

  static SaveFlightSimPath(key, fsPath, outputPath) {
    console.log('Saving Sims')
    console.log(outputPath)

    let doesExist = false;
    let data = [];
    

    if (fs.existsSync(outputPath)) {
      
      //If the File exist lets open it and add to it.
      let file = fs.readFileSync(outputPath, "utf8");

      //Read the JSON File
      var obj = JSON.parse(file);

      for (let index = 0; index < obj.length; index++) {

        //If the key is found update the value
        if (obj[index].Key === key) {
          //Update Path
          obj[index].Path = path.join(fsPath,"Effects");
          doesExist = true;
        }
      }

      //if the entry doesnt exisit add it to the existing array
      if(!doesExist){

        var entry = new FlightSimResObject(key, path.join(fsPath,"Effects"));
        obj.push(entry);
      }

      //Store the data
      data = obj;

    } else {
      // If it doesnt exist create a new array and store the new object
      data = [new FlightSimResObject(key, path)];
    }

    //Save the json file
    fs.writeFileSync(outputPath, JSON.stringify(data, null, 2));
  }

  static GetFlightSimPath(fsKey, fn) {
    //console.log(fsKey)
    var Registry = require("winreg");
    let regKey = new Registry({ hive: fsKey.Hive, key: fsKey.Path });
    regKey.valueExists(fsKey.Key, (err, exists) => {
      if (exists) {
        regKey.get(fsKey.Key, (err, item) => {
          if (err) {
            console.log(err);
          } else {
            fn(item.value);
          }
        });
      }
    });
  }

  static GetSimResult(item) {
    console.log(item.value);
  }

  static PBRProcess(json) {
    this.IsPBR = true;
    console.log("Create PBR File");
    console.log(json);

    try {
      //Check if file exists
      if (fs.existsSync(json.ObjectPath)) {
        //Create a new xFile
        let xFile = new XFile(json)
        LogSystem.Message(`File Exists ${json.ObjectPath}`);
        //Read the xFile;
        xFile.Read(json,(callBack)=>{
          LogSystem.Debug("XFile Data Reading Completed")
          LogSystem.Log(callBack);
          xFile.Compile();
        });

      }
    } catch (err) {
      console.log(err);
    }
  }

  static RunMCX(mcxPathDir, file) {
    let mcxPath = path.join(mcxPathDir, "ModelConverterX.exe");
    LogSystem.Message(`Running MCX MCXPath: ${mcxPath} || FilePath: ${file}`);
    var parameters = [file];
    console.log(parameters);
    child(mcxPath, parameters, function (err, data) {
      if (err) {
        LogSystem.ErrorMessage(err);
        return;
      }
      LogSystem.Message(data.toString());
    });
  }

//>XtoMdl /XANIM /DICT:..\..\bin\modeldef.xml /XMLSAMPLE filename.x
//%windir%\system32\cmd.exe /K E:"\FSX_SDK\SDK\Environment Kit\Modeling SDK\3DSM7\Plugins\xtomdl.exe" /DICT:E:"\FSX_SDK\SDK\Environment Kit\Modeling SDK\bin\modeldef.XML" /XMLSAMPLE > buildlog.txt

  static GenerateMDL(json){
    // let mcxPath = path.join(mcxPathDir, "ModelConverterX.exe");
    // LogSystem.Log(`Running MCX MCXPath: ${mcxPath} || FilePath: ${file}`);
    // var parameters = [file];
    // console.log(parameters);
    // child(mcxPath, parameters, function (err, data) {
    //   if (err) {
    //     LogSystem.ErrorMessage(err);
    //     return;
    //   }
    //   LogSystem.Log(data.toString());
    // });

    console.log("hi")
    const exec = require('child_process').exec;
    exec("IPCONFIG", (error, stdout, stderr) => { 
      //callback(stdout); 
      console.log(stdout)
  });
  }
}
//E:\Program Files\Lockheed Martin\Prepar3D v5 SDK 5.0.24.34874\Modeling\3ds Max\bin\modeldef.xml
//E:\Program Files\Lockheed Martin\Prepar3D v5 SDK 5.0.24.34874\Modeling\3ds Max\3DSM2019_x64\Plugins
//F:\System Folders\Desktop\C4D_Xfile_PBR_Mat\PBR_Effects_C4D.x
//F:\System Folders\Desktop\MCX Test\