var Registry = require("winreg");

export default class FlightSimulatorRegistry {
  
    constructor() {
    this.FS2004Key = {
      Path:"\\SOFTWARE\\WOW6432Node\\Microsoft\\microsoft games\\flight simulator\\9.0",
      Hive: Registry.HKLM,
      Key: "EXE Path",
    };

    this.FSXKey = {
      Path: "\\SOFTWARE\\Microsoft\\Microsoft Games\\Flight Simulator\\10.0",
      Hive: Registry.HKCU,
      Key: "AppPath",
    };

    this.FSXSEKey = {
      Path:"\\SOFTWARE\\Microsoft\\Microsoft Games\\Flight Simulator - Steam Edition\\10.0",
      Hive: Registry.HKCU,
      Key: "AppPath",
    };

    this.P3DV1Key = {
      Path: "\\SOFTWARE\\Lockheed Martin\\Prepar3D v1",
      Hive: Registry.HKLM,
      Key: "AppPath",
    };

    this.P3DV2Key = {
      Path: "\\SOFTWARE\\Lockheed Martin\\Prepar3D v2",
      Hive: Registry.HKLM,
      Key: "AppPath",
    };

    this.P3DV3Key = {
      Path: "\\SOFTWARE\\Lockheed Martin\\Prepar3D v3",
      Hive: Registry.HKLM,
      Key: "AppPath",
    };

    this.P3DV4Key = {
      Path: "\\SOFTWARE\\Lockheed Martin\\Prepar3D v4",
      Hive: Registry.HKCU,
      Key: "AppPath",
    };

    this.P3DV5Key = {
      Path: "\\SOFTWARE\\Lockheed Martin\\Prepar3D v5",
      Hive: Registry.HKLM,
      Key: "SetupPath",
    };
  }
}
