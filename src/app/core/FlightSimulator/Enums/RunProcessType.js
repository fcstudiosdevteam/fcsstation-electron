    /// <summary>
    /// They types of run commands for the station
    /// </summary>
    export let RunProcessType = {
        /// <summary>
        /// Model Converter X
        /// </summary>
        MCX: 0,
        /// <summary>
        /// FlightSim Path
        /// </summary>
        FSPaths: 1,
        /// <summary>
        /// Converts MCX Material to JSON file
        /// </summary>
        MCXMaterialToJson: 2,
        /// <summary>
        /// A Default Value
        /// </summary>
        None: -1,
    }
    export function GetProcessName(json){
        console.log(`Trying to get process name from value: ${json.ProcessType}`)
        var keyNames = Object.keys(RunProcessType);
        return keyNames[json.ProcessType];
    }